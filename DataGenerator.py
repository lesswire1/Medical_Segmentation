import numpy as np
from keras.utils import Sequence
import os
from nilearn import image as ni_image
# from scipy.misc.pilutil import imresize
import cv2
import utils
import threading


class SeqBatchGen(Sequence):
    is_opening_data = False

    def __init__(self, image_data_folder, image_label_folder, data_filenames, metadata, num_samples, batch_size,
                 slicing_mode, aug_factor, num_slices_array, slice_width, do_clahe=False):
        self.batch_size = batch_size
        self.aug_factor = aug_factor
        self.image_data_folder = image_data_folder
        self.image_label_folder = image_label_folder
        self.num_input_channels = metadata.item().get('num_input_channels')
        self.label_levels = list(metadata.item().get('output_label_values'))
        self.num_labels = len(self.label_levels)
        self.num_samples = num_samples
        self.slice_width = slice_width #metadata.item().get('slice_width')
        print("slice_width = {}".format(self.slice_width))

        self.data_filenames = data_filenames
        self.num_volumes = len(data_filenames)

        self.num_slices_array = num_slices_array
        # self.global_means = metadata.item().get('mean_input_channels')
        # self.max_data_val = metadata.item().get('max_data_val')
        # self.min_data_val = metadata.item().get('min_data_val')
        self.slicing_mode = slicing_mode
        self.clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8)) if do_clahe else None

        self.metadata = metadata
        self.levels = list(metadata.item().get('output_label_values'))

        self.cached_99percentile = {}
        self.cached_min = {}

    def __len__(self):
        return int(np.floor(float(self.num_samples) / float(self.batch_size)))

    def get_file_index_slice_index(self, img_index, batch_slicing_mode):
        if batch_slicing_mode == 0:
            cumulative_slices = 0
            file_index = 0
            for idx, num_slices in enumerate(self.num_slices_array):
                cumulative_slices += num_slices
                if img_index <= cumulative_slices:
                    file_index = idx
                    break

            file_index = file_index % len(self.data_filenames)  # just for safety
            slice_index = img_index - (cumulative_slices - self.num_slices_array[file_index])
            slice_index = slice_index % self.num_slices_array[file_index]
        else:
            num_slices_per_volume = self.slice_width
            file_index = (img_index // num_slices_per_volume) % len(self.data_filenames)
            slice_index = img_index % num_slices_per_volume
        return file_index, slice_index

    def __getitem__(self, batch_index):
        w = 224
        image_batch_shape = (self.batch_size, w, w, self.num_input_channels)
        label_batch_shape = (self.batch_size, w, w, self.num_labels)
        image_batch = np.zeros(image_batch_shape, dtype=np.float32)
        label_batch = np.zeros(label_batch_shape, dtype=np.float32)

        first_image_idx = self.batch_size * batch_index
        max_canonical_images = np.sum(self.num_slices_array)

        thresh_1 = max_canonical_images*self.aug_factor
        thresh_2 = thresh_1 + self.slice_width*self.num_volumes*self.aug_factor

        if first_image_idx <= thresh_1:
            # we are in the domain of canonical slices
            modified_first_image_idx = first_image_idx
            batch_slicing_mode = self.slicing_mode
        elif first_image_idx  <= thresh_2:
            # we are now in the domain of lateral augmented images
            modified_first_image_idx = first_image_idx - thresh_1
            # we need to cut laterally
            batch_slicing_mode = 1
        else:
            modified_first_image_idx = first_image_idx - thresh_2
            # we need to cut laterally
            batch_slicing_mode = 2

        k = 0
        image_slices_times = None
        label_slices_data = None
        prev_data_filename = None
        max_volume = 0.0
        min_volume = 0.0
        # print('batch_index = {}'.format(batch_index))
        while k < self.batch_size // self.aug_factor:  # Build batch sample by sample
            img_index = modified_first_image_idx // self.aug_factor + k

            file_index, slice_index = self.get_file_index_slice_index(img_index=img_index,
                                                                      batch_slicing_mode=batch_slicing_mode)

            assert file_index >= 0 and slice_index >= 0, 'filex_index and slice_index must be positive'
            # print('**batch_index = {}, file_index = {}, slice_index = {}, slicing_mode = {}'.format(batch_index, file_index, slice_index, batch_slicing_mode))

            data_filename = self.data_filenames[file_index]
            if data_filename != prev_data_filename:
                prev_data_filename = data_filename
                data_full_name = os.path.join(self.image_data_folder, data_filename)

                if not self.image_label_folder is None:
                    label_full_name = os.path.join(self.image_label_folder, data_filename)

                    labels_slices = ni_image.load_img(label_full_name, dtype='float32')
                    label_slices_data = labels_slices.get_data()

                image_slices_times = ni_image.load_img(data_full_name, dtype='float32')
                image_slices_times = image_slices_times.get_data()

                if data_filename in self.cached_99percentile.keys():
                    max_volume = self.cached_99percentile[data_filename]
                else:
                    max_volume = np.percentile(image_slices_times, q=95, axis=(0, 1, 2))
                    self.cached_99percentile[data_filename] = max_volume

                if data_filename in self.cached_min.keys():
                    min_volume = self.cached_min[data_filename]
                else:
                    min_volume = np.percentile(image_slices_times, q=5, axis=(0, 1, 2))
                    # min_volume = np.min(image_slices_times, axis=(0, 1, 2))
                    self.cached_min[data_filename] = min_volume

            image_slice_4ch = utils.slices_from_slices_times(image_slices_times=image_slices_times,
                                                             slice_index=slice_index,
                                                             min_volume=min_volume,
                                                             max_volume=max_volume,
                                                             slicing_mode=batch_slicing_mode,
                                                             num_time_index=self.num_input_channels,
                                                             clahe=self.clahe)

            if image_slice_4ch.shape[0] != 224 or image_slice_4ch.shape[1] != 224:
                image_slice_4ch = cv2.resize(image_slice_4ch, (w, w), interpolation=cv2.INTER_AREA)

            if len(image_slice_4ch.shape) == 2:
                image_slice_4ch = np.expand_dims(image_slice_4ch, axis=-1)

            image_batch[k*self.aug_factor] = image_slice_4ch
            # flip the image to augment the data
            if self.aug_factor >= 2:
                image_batch[k*self.aug_factor + 1] = image_slice_4ch[::-1, :, :]
            if self.aug_factor >= 3:
                image_batch[k*self.aug_factor + 2] = image_slice_4ch[:, ::-1, :]
            if self.aug_factor == 4:
                image_batch[k*self.aug_factor + 3] = image_slice_4ch[::-1, ::-1, :]

            if self.image_label_folder is not None:
                label_for_slice = utils.label_to_categorical(label_slices_data, slice_index, is_tensor=False,
                                                             levels=self.label_levels,
                                                             slicing_mode=batch_slicing_mode)
                label_batch[k*self.aug_factor] = label_for_slice

                if self.aug_factor >= 2:
                    label_batch[k*self.aug_factor + 1] = label_for_slice[::-1, :, :]
                if self.aug_factor >= 3:
                    label_batch[k*self.aug_factor + 2] = label_for_slice[:, ::-1, :]
                if self.aug_factor == 4:
                    label_batch[k*self.aug_factor + 3] = label_for_slice[::-1, ::-1, :]

            k += 1

            # label_heat, _ = utils.label_softmax_to_heat_image(label_batch[self.batch_size-1, ...], levels=self.levels, metadata=self.metadata)
            # slice_heat = utils.convert_slice_to_heatmap(image=image_slices_times[..., 0], min_value=min_volume, max_value=max_volume)
            # combo = np.hstack((slice_heat, label_heat))
            # cv2.imshow(str(batch_slicing_mode), combo)
            # cv2.waitKey(500)

            # except Exception as err:
            #     print(err)
            #     continue
        if self.image_label_folder is None:
            return image_batch
        else:
            return image_batch, label_batch





