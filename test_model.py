import cv2
from keras.models import load_model
import numpy as np
import utils
from tqdm import tqdm
from os.path import isfile, join, exists
from os import listdir, makedirs
from keras.utils.generic_utils import CustomObjectScope
from nilearn import image as ni_image
import os
import keras
import DataGenerator
from matplotlib import pyplot as plt
import functools
import math
import gc
from keras import backend as K


def compute_canonical_predictions(image_data_folder, label_folder, file_index,
                                  model, slicing_mode, do_clahe, metadata):
    # num_slices_array = metadata.item().get('num_slices_array')
    # slice_width = 224 #metadata.item().get('slice_width')
    # Load the pre-trained model

    test_filenames = [item for item in os.listdir(image_data_folder)
                      if os.path.isfile(os.path.join(image_data_folder, item))
                      and item[0] is not '.']

    test_filenames = [test_filenames[file_index]]
    # num_slices_array = [num_slices_array[file_index]]

    src_full_name = os.path.join(image_data_folder, test_filenames[0])
    image_volume_shape = ni_image.load_img(src_full_name, dtype='float32').get_data().shape[0:3]
    num_slices_array = [image_volume_shape[2]]

    # # DEBUG
    # if test_filenames[0] == 'la_001.nii.gz':
    #     src_full_name = os.path.join(image_data_folder, test_filenames[0])
    #     image_slices_times = ni_image.load_img(src_full_name, dtype='float32').get_data()
    #     print('DEBUG: image shape for {} = {}, num_slices = {}'.format(test_filenames[0], image_slices_times.shape, num_slices_array))

    if slicing_mode == 0:
        slice_width = image_volume_shape[1]   # for mode=0, slice_width does not matter
        num_test_samples = image_volume_shape[2]   # num slices
    elif slicing_mode == 1:
        slice_width = image_volume_shape[1]
        num_test_samples = slice_width * len(test_filenames)
    elif slicing_mode == 2:
        slice_width = image_volume_shape[0]
        num_test_samples = slice_width * len(test_filenames)

    # print('compute canonical predications: test_filename = {}'.format(test_filenames))

    batch_size = utils.get_bach_size_for_sample_size(num_test_samples)
    batch_size = np.min((20, batch_size))
    print('batch_size = {}'.format(batch_size))

    num_test_samples_rounded_up = math.ceil(num_test_samples / batch_size) * batch_size

    test_batch_gen = DataGenerator.SeqBatchGen(image_data_folder=image_data_folder,
                                               image_label_folder=label_folder,
                                               data_filenames=test_filenames,
                                               num_samples=num_test_samples_rounded_up,
                                               metadata=metadata,
                                               batch_size=batch_size,
                                               slicing_mode=slicing_mode,
                                               aug_factor=1,
                                               num_slices_array=num_slices_array,
                                               slice_width=slice_width,
                                               do_clahe=do_clahe)

    predictions = model.predict_generator(generator=test_batch_gen,
                                          # steps=num_steps,
                                          max_queue_size=100, workers=8, verbose=0)

    predictions = predictions[:num_test_samples]
    predictions = utils.convert_to_canonical_predictions(non_canonical_predictions=predictions,
                                                         slicing_mode=slicing_mode,
                                                         canonical_shape=image_volume_shape,
                                                         metadata=metadata)

    # print('predicted {} samples, num_test_samples = {}'.format(predictions.shape[0], num_test_samples))



    # for k, prediction in enumerate(predictions):
    #     predictions[k] = cv2.medianBlur(prediction, ksize=3)

    return predictions


# TODO: call the function below for each file_index in the folder. Need to provide a reference label (can we avoid this?)

def create_label_niImage_for_image(test_folder_224, original_test_folder, label_folder, model, file_index,
                                   metadata, do_clahe):
    levels = list(metadata.item().get('output_label_values'))
    slice_width = metadata.item().get('slice_width')

    predictions_0, predictions_1, predictions_2 = predict_labels_for_image_sample(image_data_folder=test_folder_224,
                                                                                  label_folder=label_folder,
                                                                                  model=model,
                                                                                  file_index=file_index,
                                                                                  metadata=metadata,
                                                                                  do_clahe=do_clahe)
    labels = np.empty(shape=(slice_width, slice_width, predictions_0.shape[0]))
    for slice_idx in tqdm(range(predictions_0.shape[0])):
        prediction_0 = predictions_0[slice_idx]
        prediction_1 = predictions_1[slice_idx]
        prediction_2 = predictions_2[slice_idx]

        pwr = 3.0
        combined_prediction = 3.0*np.power(prediction_0, pwr) + np.power(prediction_1, pwr) + np.power(prediction_2, pwr)
        # combined_prediction = np.power(prediction_0, pwr)

        _, combined_predicted_image_gray = utils.label_softmax_to_heat_image(combined_prediction,
                                                                             levels=levels,
                                                                             metadata=metadata)
        labels[..., slice_idx] = combined_predicted_image_gray

    label_fractions = metadata.item().get('mean_fraction_levels')
    min_fraction = np.min(label_fractions)
    kernel_size = 5 if min_fraction < 5.0e-4 else 3
    # print('fraction for rarest class is {}, kernel_size for filter3d = {}'.format(min_fraction, kernel_size))
    # labels_filtered = utils.median_filter3d(labels, kernel_size=kernel_size)
    labels_filtered = labels

    if label_folder is None:
        test_filenames = [item for item in os.listdir(original_test_folder)
                          if os.path.isfile(os.path.join(original_test_folder, item))
                          and item[0] is not '.']


        output_folder = os.path.join(test_folder_224, '..', 'labelsTs')
        if not exists(output_folder):
            makedirs(output_folder)

        output_full_name = os.path.join(output_folder, test_filenames[file_index])
        ref_image_filename = os.path.join(original_test_folder, test_filenames[file_index])
        ref_image = ni_image.load_img(ref_image_filename, dtype='float32')
        ref_image_shape = ref_image.get_data().shape
        w = ref_image_shape[0]
        h = ref_image_shape[1]
        num_samples = labels_filtered.shape[-1]
        label_resized = np.zeros(shape=(w, h, num_samples), dtype=np.uint8)
        for k in range(num_samples):
            label_resized[..., k] = cv2.resize(labels_filtered[..., k], (h, w),
                                               interpolation=cv2.INTER_NEAREST).astype(np.uint8)

        labels_ni = ni_image.new_img_like(ref_niimg=ref_image, data=label_resized, copy_header=False)
        labels_ni.to_filename(output_full_name)
        print('\nwrote label for {}'.format(ref_image_filename))

        read_back_test = ni_image.load_img(output_full_name, dtype='float32').get_data()
        print('label shape = {}, image shape = {}'.format(read_back_test.shape, ref_image_shape))
        assert ref_image_shape[:3] == read_back_test.shape, 'label size and test image size must match'
    # else:
    #     for slice_idx in tqdm(range(labels.shape[2])):
    #         slice = cv2.applyColorMap(src=labels[..., slice_idx].astype(np.uint8), colormap=cv2.COLORMAP_JET)
    #         slice_filtered = cv2.applyColorMap(src=labels_filtered[..., slice_idx].astype(np.uint8), colormap=cv2.COLORMAP_JET)
    #         combo = np.hstack((slice, slice_filtered))
    #         cv2.imwrite(filename=os.path.join(result_path, '_'+str(slice_idx)+'.png'), img=combo)

    return labels_filtered


def evaluate_labels(image_data_folder, label_folder, model, file_index,
                                   metadata, do_clahe, result_folder):

    predicted_labels = create_label_niImage_for_image(test_folder_224=image_data_folder, original_test_folder=None,
                                                      label_folder=label_folder,
                                                      model=model, file_index=file_index,
                                                      metadata=metadata, do_clahe=do_clahe)

    test_filenames = [item for item in os.listdir(image_data_folder)
                      if os.path.isfile(os.path.join(image_data_folder, item))
                      and item[0] is not '.']

    data_full_name = os.path.join(image_data_folder, test_filenames[file_index])
    image_slices_times = ni_image.load_img(data_full_name, dtype='float32')
    image_slices_times = image_slices_times.get_data()

    label_full_name = os.path.join(label_folder, test_filenames[file_index])
    labels_slices = ni_image.load_img(label_full_name, dtype='float32')
    label_slices_data = labels_slices.get_data()

    levels = list(metadata.item().get('output_label_values'))
    precision_scores = []
    dice_scores = []

    max_volume = np.percentile(image_slices_times, q=95, axis=(0, 1, 2))
    min_volume = np.percentile(image_slices_times, q=5, axis=(0, 1, 2))
    # min_volume = np.min(image_slices_times, axis=(0, 1, 2))
    # print('min = {}, max = {}'.format(min_volume, max_volume))

    print('predicted labels shape: {}, image shape: {}, image name: {}'.format(predicted_labels.shape,
                                                                               image_slices_times.shape, data_full_name))
    for slice_idx in tqdm(range(image_slices_times.shape[2])):
        if len(image_slices_times.shape) == 4:
            time_index = 0
            image_slices_for_time = image_slices_times[..., time_index]
            min_value = min_volume[time_index]
            max_value = max_volume[time_index]
        else:
            min_value = min_volume
            max_value = max_volume
            image_slices_for_time = image_slices_times

        image_slice = image_slices_for_time[:, :, slice_idx]
        mask = image_slice == 0
        img = utils.convert_slice_to_heatmap(image=image_slice,
                                             min_value=min_value, max_value=max_value,
                                             should_normalize=False)
        img[mask] = 0

        predicted_255 = (predicted_labels[..., slice_idx] / np.max(levels) * 255.0).astype(np.uint8)
        predicted_label = cv2.applyColorMap(predicted_255, cv2.COLORMAP_JET)

        gt_image_3ch = utils.label_to_heat(label_slices_data, slice_idx, levels=levels)
        composite_img = np.hstack((img, predicted_label, gt_image_3ch))

        composite_name = os.path.join(result_folder, str(slice_idx) + '.png')

        label_data = label_slices_data[..., slice_idx].astype(np.uint8)
        label_data = np.reshape(label_data, newshape=(1, -1))
        label_data = np.squeeze(label_data)

        slice_predicted_labels = predicted_labels[..., slice_idx]
        slice_predicted_labels = np.reshape(slice_predicted_labels, newshape=(1, -1))
        slice_predicted_labels = np.squeeze(slice_predicted_labels)

        precision = utils.compute_score(predicted_label=slice_predicted_labels, gt_label=label_data)
        precision_scores.append(precision)
        dice_score = utils.compute_dice_multi_label(y_true=label_data, y_pred=slice_predicted_labels, levels=levels)
        dice_scores.append(dice_score)

        np.set_printoptions(precision=3)
        text = 'precision = {:1.4f}, dice = {}'.format(precision, np.array(dice_score))
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(composite_img, text, org=(10, 20), fontFace=font, fontScale= 0.5, color=(255, 255, 255))

        cv2.imwrite(composite_name, composite_img)

    mean_precision = np.mean(precision_scores)
    mean_dice_score = np.nanmean(dice_scores, axis=0)
    message = 'precision = {:1.4f}, dice = {}'.format(mean_precision, mean_dice_score)
    print('---------------------------')
    print(message)
    print('---------------------------')
    utils.notification(message)



def predict_labels_for_image_sample(image_data_folder, label_folder, model, file_index, metadata, do_clahe):

    predictions_0 = compute_canonical_predictions(image_data_folder, label_folder, metadata=metadata,
                                                  file_index=file_index,
                                                  model=model, slicing_mode=0, do_clahe=do_clahe)

    predictions_1 = compute_canonical_predictions(image_data_folder, label_folder, metadata=metadata,
                                                  file_index=file_index,
                                                  model=model, slicing_mode=1, do_clahe=do_clahe)

    predictions_2 = compute_canonical_predictions(image_data_folder, label_folder, metadata=metadata,
                                                  file_index=file_index,
                                                  model=model, slicing_mode=2, do_clahe=do_clahe)

    return predictions_0, predictions_1, predictions_2


def predict_labels_for_validation_sample(image_data_folder, label_folder, file_index,
                                         metadata, result_folder, model, do_clahe):
    predictions_0, predictions_1, predictions_2 = predict_labels_for_image_sample(image_data_folder=image_data_folder,
                                                                                  label_folder=label_folder,
                                                                                  model=model,
                                                                                  file_index=file_index,
                                                                                  metadata=metadata,
                                                                                  do_clahe=do_clahe)
    levels = list(metadata.item().get('output_label_values'))

    test_filenames = [item for item in os.listdir(image_data_folder)
                      if os.path.isfile(os.path.join(image_data_folder, item))
                      and item[0] is not '.']

    data_full_name = os.path.join(image_data_folder, test_filenames[file_index])
    image_slices_times = ni_image.load_img(data_full_name, dtype='float32')

    label_full_name = os.path.join(label_folder, test_filenames[file_index])
    labels_slices = ni_image.load_img(label_full_name, dtype='float32')
    label_slices_data = labels_slices.get_data()

    precision_scores = []
    dice_scores = []
    combined_precision_scores = []
    combined_dice_scores = []
    print('writing images...')
    for slice_idx in tqdm(range(predictions_0.shape[0])):
        prediction_0 = predictions_0[slice_idx]
        prediction_1 = predictions_1[slice_idx]
        prediction_2 = predictions_2[slice_idx]

        pwr = 0.50
        combined_prediction = np.power(prediction_0, pwr) + np.power(prediction_1, pwr) + np.power(prediction_2, pwr)
        # combined_prediction = predictions_0

        predicted_0_image_3ch, _ = utils.label_softmax_to_heat_image(prediction_0, levels=levels, metadata=metadata)
        predicted_1_image_3ch, _ = utils.label_softmax_to_heat_image(prediction_1, levels=levels, metadata=metadata)
        predicted_2_image_3ch, _ = utils.label_softmax_to_heat_image(prediction_2, levels=levels, metadata=metadata)
        combined_predicted_image_3ch, _ = utils.label_softmax_to_heat_image(combined_prediction,
                                                                            levels=levels, metadata=metadata)

        image_slices_for_time = ni_image.index_img(image_slices_times, 0).get_data()
        image_slice = image_slices_for_time[:, :, slice_idx]
        min_value = metadata.item().get('min_data_val')
        max_value = metadata.item().get('max_data_val')
        img = utils.convert_slice_to_heatmap(image=image_slice,
                                             min_value=min_value, max_value=max_value,
                                             should_normalize=False)

        gt_image_3ch = utils.label_to_heat(label_slices_data, slice_idx, levels=levels)
        composite_img = np.hstack((img, predicted_0_image_3ch, predicted_1_image_3ch, predicted_2_image_3ch,
                                   combined_predicted_image_3ch, gt_image_3ch))

        composite_name = os.path.join(result_folder, str(slice_idx) + '.png')

        # compute precision score
        combined_predicted_labels = utils.softmax_to_labels(combined_prediction, levels=levels, metadata=metadata)
        predicted_labels = utils.softmax_to_labels(prediction_0, levels=levels, metadata=metadata)

        # FIXME: account for slice laterally if you want to train with lateral slicing
        label_data = label_slices_data[..., slice_idx]
        label_data = np.reshape(label_data, newshape=(1, -1))
        label_data = np.squeeze(label_data)

        precision = utils.compute_score(predicted_label=predicted_labels, gt_label=label_data)
        precision_scores.append(precision)
        dice_score = utils.compute_dice_multi_label(y_true=label_data, y_pred=predicted_labels, levels=levels)
        dice_scores.append(dice_score)

        precision_combined = utils.compute_score(predicted_label=combined_predicted_labels, gt_label=label_data)
        combined_precision_scores.append(precision_combined)
        combined_dice_score = utils.compute_dice_multi_label(y_true=label_data,
                                                             y_pred=combined_predicted_labels, levels=levels)
        combined_dice_scores.append(combined_dice_score)

        np.set_printoptions(precision=3)

        text = 'precision = {:1.4f}, combined precision = {:1.4f}, dice = {}, combined dice = {}'.format(
            precision, precision_combined, np.array(dice_score), np.array(combined_dice_score))
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(composite_img, text, org=(10, 20), fontFace=font, fontScale= 0.5, color=(255, 255, 255))
        cv2.imwrite(composite_name, composite_img)


    mean_precision = np.mean(precision_scores)
    mean_dice_score = np.nanmean(dice_scores, axis=0)
    message = 'mean precision = {:1.4f}, mean_dice = {}'.format(mean_precision, mean_dice_score)
    print(message)
    utils.notification(message)

    mean_combined_precision = np.mean(combined_precision_scores)
    mean_combined_dice_score = np.mean(combined_dice_scores, axis=0)
    message = 'mean combined precision = {:1.4f}, mean_dice = {}'.format(mean_combined_precision, mean_combined_dice_score)
    print(message)

    # plt.figure()
    # plt.hist(precision_scores, bins=50, normed=True)
    # plt.pause(0.001)


if __name__ == "__main__":

    task_to_test = 7
    image_data_folder_, label_folder_, metadata, test_folder, test_folder_224 = utils.get_folders_for_task(task_index=task_to_test-1,
                                                                           tasks_path='D:/ML_Data')

    model_path = '../Decalon/checkpoints/task_7_rc3.h5'
    # model_path = './models/Final/task_' + str(task_to_test) + '.h5'
    # model_path = './models/older/task_' + str(task_to_test) + '.h5'

    result_path = '../Decalon/results_debug'
    if not exists(result_path):
        makedirs(result_path)

    # model_path = './models/noDice_89percent.h5'
    # model_path = '../Decalon/checkpoints/heart_noAug_80percent.h5'
    # model_path = '../Decalon/checkpoints/8.h5'
    # model_path = './models/clahe_w1222_89percent.h5'
    # model_path = './models/CE_Dice_w1222.h5'

    # predict_labels_for_validation_sample(image_data_folder=image_data_folder,
    #                                      label_folder=label_folder,
    #                                      file_index=-5,
    #                                      metadata=metadata,
    #                                      result_folder=result_path,
    #                                      model_path=model_path,
    #                                      do_clahe=True)

    # predicted_labels = create_label_niImage_for_image(image_data_folder=image_data_folder,
    #                                model_path=model_path, file_index=-5, ref_label_filename=None,
    #                                output_folder=result_path, metadata=metadata, do_clahe=False)


    print('loading the model from {}'.format(model_path))
    label_fractions = metadata.item().get('mean_fraction_levels')
    class_weights = np.exp(-label_fractions*5.0)*100.0
    partial_loss = functools.partial(utils.pixelwise_crossentropy, class_weights=class_weights)
    partial_loss.__name__ = "partial_loss"


    def relu6(x):
        return K.relu(x, max_value=6)

    with CustomObjectScope({'relu6': relu6,
                            'DepthwiseConv2D': keras.layers.DepthwiseConv2D,
                            'partial_loss': partial_loss,
                            'tversky_loss': utils.tversky_loss,
                            'pixelwise_crossentropy': utils.pixelwise_crossentropy}):
        model = load_model(model_path)



    evaluate_labels(image_data_folder=image_data_folder_, label_folder=label_folder_,
                    model=model, file_index=-4,
                    metadata=metadata, do_clahe=False, result_folder=result_path)



    # # creating test image
    # test_filenames = [item for item in os.listdir(test_folder)
    #                   if os.path.isfile(os.path.join(test_folder, item))
    #                   and item[0] is not '.']
    # print('predicting labels for test image for teask {}'.format(task_to_test))
    # for file_index in tqdm(range(len(test_filenames))):
    #     _ = create_label_niImage_for_image(test_folder_224=test_folder_224, original_test_folder=test_folder,
    #                                        label_folder=None, model=model,
    #                                        file_index=file_index, metadata=metadata, do_clahe=False)