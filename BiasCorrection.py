from nilearn import image as ni_image
import numpy as np
import cv2
from nipype.interfaces.ants import N4BiasFieldCorrection
from matplotlib import pyplot as plt
import os
from tqdm import tqdm



def n4_simple():
    image_path = 'newbiascorr3317.nii.gz' #''./BRATS_484.nii.gz'
    img = sitk.ReadImage(image_path)
    # data = sitk.GetArrayFromImage(img)
    img_data = sitk.Cast(img, sitk.sitkFloat32)
    img_mask = sitk.BinaryNot(sitk.BinaryThreshold(img_data, 0, 0))
    print('starting N4...')
    corrected_img = sitk.N4BiasFieldCorrection(img_data, img_mask)
    print('Done with N4!')
    # mask = './mask.jpg'
    # sitk.WriteImage(img_mask, mask)
    new_img = './BRATS_484_corrected.nii.gz'
    sitk.WriteImage(corrected_img, new_img)
    print("Finished N4 Bias Field Correction.....")
    write_image(image_path, './original_slice.png')
    write_image(new_img, './corrected_slice.png')


def n4_normalize(input_image_path, output_image_path):

    filenames = [item for item in os.listdir(image_data_folder)
                       if os.path.isfile(os.path.join(image_data_folder, item))
                       and item[0] is not '.']

    if not os.path.exists(output_image_path):
        os.makedirs(output_image_path)

    N4BiasFieldCorrection.set_default_num_threads(16)
    for filename in tqdm(filenames):
        input_image_full_path = os.path.join(input_image_path, filename)
        out_image_full_path = os.path.join(output_image_path, filename)

        n4 = N4BiasFieldCorrection(output_image=out_image_full_path)
        n4.set_default_num_threads(16)
        # dimension of input slice, input slice
        n4.inputs.dimension = 4
        n4.inputs.input_image = input_image_full_path
        n4.inputs.shrink_factor = 3
        # n4.inputs.n_iterations = [50, 50, 30, 20]
        n4.inputs.n_iterations = [10, 10, 10, 10]
        n4.inputs.convergence_threshold = 1e-3

        # print(n4.cmdline)
        n4.run()


def write_image(image_path, image_name):
    slice_index = 75
    image_slices_times = ni_image.load_img(image_path, dtype='float32')
    images = []
    for time_index in range(4):
        image_slices_for_time = ni_image.index_img(image_slices_times, time_index)
        image_slices_for_time_data = image_slices_for_time.get_data()  # 3D tensor
        image_slice_data = image_slices_for_time_data[..., slice_index]  # 2D tensor
        image_slice_data = np.array(image_slice_data) / 1461.0 * 255

        no_zeros = image_slice_data
        # no_zeros[no_zeros == 0] = np.nan
        #
        images.append(image_slice_data)
        #
        # print(time_index)
        # plt.figure()
        # x = np.reshape(image_slice_data, newshape=(1, -1))
        # x = np.squeeze(x)
        #
        # m = np.nanmean(no_zeros)
        # print('mean = {}'.format(m))

        # plt.hist(x, bins=50)
        # plt.title('mean = {}'.format(m))
        # plt.show()

    composite = np.hstack(images)
    cv2.imwrite(image_name, composite)


if __name__=='__main__':
    os.environ['PATH'] = '/Users/ramin/anaconda3/envs/Brain_Segmentation/bin:/Library/Frameworks/Python.framework/Versions/3.6/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Applications/Wireshark.app/Contents/MacOS:/Users/ramin/Documents/Developer/ANTs/build/bin'

    image_data_folder = './Data/Task01_BrainTumour/imagesTr'
    filenames = [os.path.join(image_data_folder, item) for item in os.listdir(image_data_folder)
                       if os.path.isfile(os.path.join(image_data_folder, item))
                       and item[0] is not '.']

    output_image_path = './Data/Task01_BrainTumour/imagesTr_N4_corrected'
    n4_normalize(input_image_path=image_data_folder, output_image_path=output_image_path)


    # image_path = './Data/callback_test/data/BRATS_484.nii.gz'
    # write_image(image_path, './junk/composite.png')
    #
    # image_path = './junk/BRATS_484_corrected.nii.gz'
    # write_image(image_path, './junk/composite_corrected.png')
