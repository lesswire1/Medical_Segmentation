from train import model_fit
import random
import utils
import numpy as np

random.seed(19)
np.random.seed(19)

task_to_run = 7

image_data_folder, label_folder, metadata, _, _ = utils.get_folders_for_task(task_index=task_to_run-1,
                                                                       tasks_path='D:/ML_Data')

model_path = '../Decalon/checkpoints/resume_5.h5'
model_path = './models/Final/task_7.h5'
# model_path = '../Decalon/checkpoints/3.h5'
model_fit(batch_size=20, log_name='task7_Aug2_lr1e4_w05_k5_noDO',
          should_load_model=False, model_path=model_path,
          image_data_folder=image_data_folder,
          label_folder=label_folder,
          metadata=metadata,
          slicing_mode=0,
          is_debug=False,
          do_clahe=False)

