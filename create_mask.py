import cv2
from keras.models import load_model
import numpy as np
import utils
from tqdm import tqdm
from os.path import isfile, join, exists
from os import listdir, makedirs
from keras.utils.generic_utils import CustomObjectScope
from nilearn import image as ni_image
import os
import keras


# environ['CUDA_VISIBLE_DEVICES'] = '0'


def create_labels(input_image_folder, label_folder, result_folder, model_path):
    filename = os.listdir(input_image_folder)[0]

    data_full_name = os.path.join(input_image_folder, filename)
    label_full_name = os.path.join(label_folder, filename)

    image_slices_times = ni_image.load_img(data_full_name, dtype='float32')
    labels_slices = ni_image.load_img(label_full_name, dtype='float32')
    label_slices_data = labels_slices.get_data()

    time_index = 0
    image_slices = ni_image.index_img(image_slices_times, time_index)
    image_slices_data = image_slices.get_data()

    w = 224
    image_batch_shape = (10, w, w, 3)
    label_batch_shape = (10, w, w, 1)

    image_batch = np.zeros(image_batch_shape, dtype=np.float32)
    label_batch = np.zeros(label_batch_shape, dtype=np.float32)

    k = 0
    for slice_index in range(70, 74):
        image_slice = image_slices_data[..., slice_index]
        image_slice /= 1461.0
        image_slice -= 0.5
        image_slice = np.expand_dims(image_slice, 2)
        image_slice_3ch = np.repeat(image_slice, repeats=3, axis=2)
        image_batch[k] = cv2.resize(image_slice_3ch, (w, w))

        label_data = label_slices_data[..., slice_index]
        label_data = cv2.resize(label_data, (w, w))
        label_data = np.expand_dims(label_data, 2)
        label_batch[k] = label_data

        k += 1

    # Load the pre-trained model
    with CustomObjectScope({'relu6': keras.applications.mobilenet.relu6,
                            'DepthwiseConv2D': keras.applications.mobilenet.DepthwiseConv2D}):
        model = load_model(model_path)

        predicted_labels = model.predict(image_batch)
    print('predicted_label.shape = {}'.format(predicted_labels.shape))
    print('writing results to {}'.format(result_folder))
    for i in tqdm(range(predicted_labels.shape[0])):
        img = image_batch[i, :, :, 0] + 0.5
        composite_img = np.hstack((img * 255.0,
                                   255.0 / 4.0 * predicted_labels[i, :, :, 0]*10000,
                                   255.0 / 4.0 * label_batch[i, :, :, 0]))
        filename = os.path.join(result_folder, str(i) + '.png')
        cv2.imwrite(filename, composite_img)
        print(predicted_labels[i, :, :, 0])


if __name__ == "__main__":
    data_dir = './Data/callback_test/data'
    label_dir = './Data/callback_test/label'
    result_path = './results'
    if not exists(result_path):
        makedirs(result_path)

    model_path = './checkpoints/model_at_epoch_4.h5'
    create_labels(input_image_folder=data_dir, label_folder=label_dir,
                  result_folder=result_path, model_path=model_path)

