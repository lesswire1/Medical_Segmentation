from nilearn import image as NI_image
import os
import cv2
import h5py
from tqdm import tqdm

if __name__ == "__main__":
    data_folder = './Data/Task01_BrainTumour/imagesTr'
    label_folder = './Data/Task01_BrainTumour/labelsTr'

    df5_filename = './Data/Task01_BrainTumour.df5'
    dataset_name = 'Task01_BrainTumour'

    nii_files = [item for item in os.listdir(data_folder) if os.path.isfile(os.path.join(data_folder, item))
                 and item[0] is not '.']

    data_shape = (len(nii_files)*155*4, 240, 240)

    with h5py.File(df5_filename, "a") as f:
        idx = 0
        df5_data = f.create_dataset(dataset_name, dtype='int16', shape=data_shape)

        for nii_file in tqdm(nii_files[:3]):
            nii_full_name = os.path.join(data_folder, nii_file)
            # index = nii_file.split('_')[1]
            label_full_name = os.path.join(label_folder, nii_file)

            image = NI_image.load_img(nii_full_name)
            label = NI_image.load_img(label_full_name)

            for t in range(image.shape[3]):
                image_t = NI_image.index_img(image, t)
                image_data = image_t.get_data()

                label_data = label.get_data()

                for s in range(image.shape[2]):
                    image_s = image_data[:, :, s]
                    cv2.imwrite('./junk/'+str(t)+'_'+str(s)+'.jpg', image_s/1461.0*255.0)
                    df5_data[idx, ...] = image_s


