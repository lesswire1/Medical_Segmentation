# from __future__ import division
from keras import initializers
# from keras.applications.resnet50 import ResNet50, decode_predictions
from keras.preprocessing import image
from keras.preprocessing.sequence import pad_sequences
from keras.models import Model, Sequential
from keras.layers import *
from keras.optimizers import Adam
from keras.regularizers import l2
from keras.utils.data_utils import get_file
from keras.applications.imagenet_utils import decode_predictions, preprocess_input
import keras
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
import tensorflow as tf
import random
import scipy.misc
import numpy as np
import os
from tqdm import tqdm
from os.path import isfile, join, isdir, exists
from os import listdir, makedirs, remove
import cv2
from nilearn import image as ni_image
from sklearn import metrics
import requests
import Metadata_Generator
import math
from sklearn.utils import class_weight


import warnings
# from PIL import Image
# warnings.simplefilter('ignore', Image.DecompressionBombWarning)


def divisorGenerator(n):
    large_divisors = []
    for i in range(1, int(math.sqrt(n) + 1)):
        if n % i == 0:
            yield i
            if i*i != n:
                large_divisors.append(n / i)
    for divisor in reversed(large_divisors):
        yield divisor


def get_bach_size_for_sample_size(sample_size):
    divisors = np.array(list(divisorGenerator(sample_size)))
    idx = np.where(np.logical_and(divisors >= 10, divisors <= sample_size))
    if len(divisors[idx]) > 1:
        batch_size = int(divisors[idx][0])
    else:
        batch_size = int(divisors[idx])
    return batch_size

def median_filter3d(volume, kernel_size=3):
    # volume_ is a 3D tensor. It represents single-channel lables
    volume_ = np.copy(volume)
    num_slices = volume_.shape[2]
    for k in range(num_slices):
        slice = volume_[:, :, k].astype(np.uint8)
        volume_[:, :, k] = (cv2.medianBlur(slice, ksize=kernel_size)).astype(np.uint8)
        # volume_[:, :, k] = cleanup_image(slice)

    num_slices = volume_.shape[1]
    for k in range(num_slices):
        slice = volume_[:, k, :].astype(np.uint8)
        volume_[:, k, :] = (cv2.medianBlur(slice, ksize=kernel_size)).astype(np.uint8)
        # volume_[:, k, :] = cleanup_image(slice)

    num_slices = volume_.shape[0]
    for k in range(num_slices):
        slice = volume_[k, :, :].astype(np.uint8)
        volume_[k, :, :] = (cv2.medianBlur(slice, ksize=kernel_size)).astype(np.uint8)
        # volume_[k, :, :] = cleanup_image(slice)

    return volume_


def get_folders_for_task(task_index, tasks_path='D:/ML_Data'):

    task_names = [item for item in os.listdir(tasks_path)
                  if os.path.isdir(os.path.join(tasks_path, item))
                  and 'Task' in item]

    task_name = task_names[task_index]
    print('training {}...'.format(task_name))

    task_path = os.path.join(tasks_path, task_name)

    metadata_filename = os.path.join(tasks_path, 'metadata', 'meta_' + task_name + '.npy')
    if not os.path.exists(metadata_filename):
        print('Generating metadata for task {}...'.format(task_name))
        Metadata_Generator.generate_metadata_for_task(task_path=task_path,
                                                      metadata_path=os.path.join(tasks_path, 'metadata'))
    else:
        print('loading existing metadata...')
    metadata = np.load(metadata_filename)

    image_data_folder = os.path.join(tasks_path, task_name, 'imagesTr_224_u')
    label_folder = os.path.join(tasks_path, task_name, 'labelsTr_224_u')
    test_data_folder = os.path.join(tasks_path, task_name, 'imagesTs')
    test_data_folder_224 = os.path.join(tasks_path, task_name, 'imagesTs_224_u')

    return image_data_folder, label_folder, metadata, test_data_folder, test_data_folder_224


def combine_softmax_predictions(pred_1, pred_2):
    return  pred_1 + pred_2


def do_clahe(slice, clahe):
    print('********* CLAHE is called ************')
    slice = np.array(slice/1461.0*255.0).astype(np.uint8)
    slice = np.array(slice/1461.0*255.0).astype(np.uint8)
    slice = (clahe.apply(slice)).astype(np.float32)/255.0*1461.0
    return slice


def convert_to_canonical_predictions(non_canonical_predictions, slicing_mode, metadata, canonical_shape):
    slice_width = 224 #metadata.item().get('slice_width')
    num_output_channels = len(metadata.item().get('output_label_values'))
    # num_slices_array = metadata.item().get('num_test_slices_array')
    num_canonical_slices = canonical_shape[2]

    print('non-canonical prediction shape: {}'.format(non_canonical_predictions.shape))
    num_samples = non_canonical_predictions.shape[0]
    canonical_prediction = np.empty(shape=(canonical_shape[2], slice_width, slice_width,
                                           num_output_channels), dtype=np.float32)
    print('converting to canonical...')
    for k in tqdm(range(num_samples)):
        # this block is related to lateral slicing only
        # volume_index = k // slice_width
        # num_slices_for_this_volume = num_slices_array[volume_index]
        # start = int(np.sum(num_slices_array[:volume_index]))
        # stop = int(np.sum(num_slices_array[:volume_index + 1]))
        start = 0
        stop = num_canonical_slices

        if slicing_mode == 0:
            prediction = non_canonical_predictions[k, ...]
            canonical_prediction[k, :, :, :] = cv2.resize(prediction, (slice_width, slice_width),
                                                          interpolation=cv2.INTER_NEAREST)
        elif slicing_mode == 1:
            prediction = non_canonical_predictions[num_samples - 1 - k, ...]
            prediction = np.moveaxis(prediction, [0, 1], [1, 0])
            canonical_prediction[start:stop, k, :, :] = cv2.resize(prediction, (slice_width, num_canonical_slices),
                                                          interpolation=cv2.INTER_NEAREST)
        elif slicing_mode == 2:
            prediction = non_canonical_predictions[num_samples - 1 - k, ...]
            prediction = np.moveaxis(prediction, [0, 1], [1, 0])
            canonical_prediction[start:stop, :, k, :] = cv2.resize(prediction, (slice_width, num_canonical_slices),
                                                          interpolation=cv2.INTER_NEAREST)
    print('canonical prediction shape: {}'.format(non_canonical_predictions.shape))

    num_samples = canonical_prediction.shape[0]
    if slicing_mode == 1:
        for k in range(num_samples):
            canonical_prediction[k, :, :, :] = np.moveaxis( canonical_prediction[k, :, :, :],  [0, 1], [1, 0])
            canonical_prediction[k, :, :, :] = canonical_prediction[k, :, ::-1, :]
    elif slicing_mode == 2:
        for k in range(num_samples):
            canonical_prediction[k, :, :, :] = np.moveaxis( canonical_prediction[k, :, :, :],  [0, 1], [1, 0])
            canonical_prediction[k, :, :, :] = canonical_prediction[k, ::-1, :, :]

    return canonical_prediction


def compute_dice_score(y_true, y_pred):
    '''Dice Similarity Score for binary images'''
    s = np.sum(y_pred) + np.sum(y_true)
    if s == 0:
        return np.nan
    else:
        dice = np.sum(y_pred[y_true > 0]) * 2.0 / s
        return dice


def compute_dice_multi_label(y_true, y_pred, levels):
    scores = []
    for level in levels:
        y_true_1ch = (y_true == level)
        y_pred_1ch = (y_pred == level)
        score = compute_dice_score(y_true=y_true_1ch, y_pred=y_pred_1ch)
        scores.append(score)
    return scores


def dice_loss(y_true, y_pred, class_weights):
    s = 6.8
    # class_weights = np.power(class_weights, 2)
    # class_weights = [0.8/s, 2.0/s, 2.0/s, 2.0/s]  # was 1.0
    class_weights = [1.0, 1000.0]
    # computed the sum across w and h
    num = tf.reduce_sum(y_true * y_pred, axis=[1, 2], keepdims=False) + K.epsilon()
    denum = tf.reduce_sum(y_true + y_pred, axis=[1, 2], keepdims=False) + K.epsilon()

    dice_loss = tf.log(2.0 - 2.0*num / denum)
    # dice = 2.0*num / denum
    # eps = 1.0e-4
    # alpha = -2.0
    # dice_loss = tf.pow((1+eps)/(dice + eps), alpha) * 1000.0 # / (10 ** (alpha + 10))

    dice_loss_across_channels = tf.reduce_mean(class_weights * dice_loss, axis=-1)  # weighted mean across classes
    return tf.reduce_mean(dice_loss_across_channels) / 1000.0 # mean across samples


def notification(message):
    report = {}
    report['value1'] = message
    requests.post('https://maker.ifttt.com/trigger/new_iteration/with/key/ElqVuK21bUtp0qxa--LNH', data=report)


def abs_KL_div(y_true, y_pred, class_weights):
    y_true = K.clip(y_true, K.epsilon(), None)
    y_pred = K.clip(y_pred, K.epsilon(), None)
    return K.sum(class_weights * K.abs((y_true - y_pred) * (K.log(y_true / y_pred))), axis=-1)


def pixelwise_crossentropy(y_true, y_pred, class_weights):
    # return abs_KL_div(y_true=y_true, y_pred=y_pred, class_weights=class_weights)

    # dice = dice_loss(y_true=y_true, y_pred=y_pred, class_weights=class_weights)
    # return dice

    y_pred = K.clip(y_pred, K.epsilon(), None)

    # num_points_in_class = tf.reduce_sum(y_true, axis=(1, 2), keep_dims=True)
    # cross_entropy_across_channels = tf.reduce_sum( y_true * tf.log(y_pred) / (1.0 + num_points_in_class), axis=-1)
    # loss = -tf.reduce_sum(cross_entropy_across_channels)


    #  see focal loss: https://arxiv.org/abs/1708.02002
    # gamma = 0.50  # was 1.5
    # cross_entropy_across_channels = tf.reduce_sum(class_weights * K.pow(1.0 - y_pred, gamma) * y_true * tf.log(y_pred),
    #                                               axis=-1)

    cross_entropy_across_channels = tf.reduce_sum(class_weights * y_true * tf.log(y_pred), axis=-1)

    loss = -tf.reduce_mean(cross_entropy_across_channels)

    # class_weights = np.power(class_weights, 2)/10000.0
    # loss_2 = tf.reduce_mean(tf.reduce_sum(class_weights * tf.pow(y_true - y_pred, 2), axis=-1))
    return loss


# Ref: salehi17, "Twersky loss function for image segmentation using 3D FCDN"
# -> the score is computed for each class separately and then summed
# alpha=beta=0.5 : dice coefficient
# alpha=beta=1   : tanimoto coefficient (also known as jaccard)
# alpha+beta=1   : produces set of F*-scores
# implemented by E. Moebel, 06/04/18
def tversky_loss(y_true, y_pred):
    alpha = 0.2
    beta = 0.8

    ones = K.ones_like(y_true)
    p0 = y_pred  # proba that voxels are class i
    p1 = ones - y_pred  # proba that voxels are not class i
    g0 = y_true
    g1 = ones - y_true

    axis = (0, 1, 2)
    num = K.sum(p0 * g0, axis)
    den = num + alpha * K.sum(p0 * g1, axis) + beta * K.sum(p1 * g0, axis)

    T = K.sum(num / den)  # when summing over classes, T has dynamic range [0 Ncl]

    Ncl = K.cast(K.shape(y_true)[-1], 'float32')
    return Ncl - T


# def total_loss(y_true, y_pred):
#     x_entropy = pixelwise_crossentropy(y_true, y_pred)
#     # dice = dice_loss(y_true, y_pred)
#     return x_entropy


def depth_softmax(matrix, is_tensor=True):
    # increase temperature to make the softmax more sure of itself
    temp = 2.0
    # print('input to sigmoid = {}'.format(matrix[0, 0, :]))

    if is_tensor:
        exp_matrix = K.exp(matrix/temp)
        softmax_matrix = exp_matrix / K.sum(exp_matrix, axis=-1, keepdims=True)
    else:
        exp_matrix = np.exp(matrix/temp)
        softmax_matrix = exp_matrix / np.sum(exp_matrix, axis=-1, keepdims=True)

    return softmax_matrix


def label_to_heat(label_slices_data, slice_index, levels):
    label_data = label_slices_data[:, :, slice_index]
    gray_label = (label_data / np.max(levels) * 255.0).astype(np.uint8)
    heat = cv2.applyColorMap(gray_label, cv2.COLORMAP_JET)
    return heat


def label_to_categorical(label_slices_data, slice_index, slicing_mode, levels, is_tensor=True):
    w = 224
    if slicing_mode == 0:
        slice_index = slice_index % label_slices_data.shape[2]
        label_data = label_slices_data[:, :, slice_index]
    elif slicing_mode == 1:
        slice_index = slice_index % label_slices_data.shape[1]
        label_data = label_slices_data[:, slice_index, :]
    elif slicing_mode == 2:
        slice_index = slice_index % label_slices_data.shape[0]
        label_data = label_slices_data[slice_index, :, :]

    if label_data.shape[0] != 224 or label_data.shape[1] != 224 :
        label_data = cv2.resize(label_data, (w, w), interpolation=cv2.INTER_NEAREST)
    label_data = np.expand_dims(label_data, 2)

    num_levels = len(levels)
    label_classes = np.zeros((w, w, num_levels))
    k = 0
    for level in levels:
        mask = (label_data == level)*1.0
        mask = np.squeeze(mask, axis=2)
        label_classes[:, :, k] = mask
        k += 1

    # labels_sigmoid = depth_softmax(label_classes, is_tensor=is_tensor)
    # return labels_sigmoid
    return label_classes


def label_softmax_to_heat_image(labels_softmax, levels, metadata):
    slice_width = metadata.item().get('slice_width')
    gray_label = np.zeros_like(labels_softmax[..., 0])
    arg_max = np.argmax(labels_softmax, axis=-1)
    for k in range(len(levels)):
        mask = arg_max == k
        gray_label[mask] = levels[k]

    gray_label = cv2.resize(gray_label, (slice_width, slice_width), interpolation=cv2.INTER_NEAREST)

    # gray_label = cleanup_image(gray_label)
    gray_label = cv2.medianBlur(gray_label, 3)

    gray_label_255 = (gray_label / np.max(levels) * 255.0).astype(np.uint8)
    gray_label = gray_label.astype(np.uint8)
    heat = cv2.applyColorMap(gray_label_255, cv2.COLORMAP_JET)
    return heat, gray_label


def softmax_to_labels(labels_probability, levels, metadata):
    slice_width = metadata.item().get('slice_width')
    gray_label = np.zeros_like(labels_probability[..., 0])
    arg_max = np.argmax(labels_probability, axis=2)
    for k in range(len(levels)):
        mask = arg_max == k
        gray_label[mask] = levels[k]

    gray_label = cv2.resize(gray_label, (slice_width, slice_width), interpolation=cv2.INTER_NEAREST)

    # gray_label = cleanup_image(gray_label)
    gray_label = cv2.medianBlur(gray_label, 3)

    gray_label = np.reshape(gray_label, newshape=(1, -1))
    gray_label = np.squeeze(gray_label)

    return gray_label


def cleanup_image(img):
    kernel = np.ones((3, 3), np.uint8)
    img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
    return img


def compute_score(predicted_label, gt_label):
    assert len(predicted_label) == len(gt_label), \
        'prediction and label must have the same number of samples: {} != {}'.format(len(predicted_label), len(gt_label))
    score = metrics.precision_score(y_true=gt_label, y_pred=predicted_label, average='macro')
    return score


def mean_slice(slice):
    non_zero = slice
    if np.mean(slice) == 0.0:
        return 0.0
    else:
        # replace zeros with nan so that we don't count them in mean calculation
        non_zero[slice == 0] = np.nan
        return np.nanmean(non_zero)


def mean_volume(volume):
    non_zero = volume
    if np.mean(volume) == 0.0:
        return 0.0
    else:
        # replace zeros with nan so that we don't count them in mean calculation
        non_zero[volume == 0] = np.nan
        return np.nanmean(non_zero)

def compute_mean_slices(image_slices_times):
    mean_slices = [0.0, 0.0, 0.0, 0.0]
    for time_index in range(4):
        k = 0.0
        image_slices_for_time = ni_image.index_img(image_slices_times, time_index)
        image_slices_for_time_data = image_slices_for_time.get_data()  # 3D tensor
        for slice_index in range(155):
            image_slice_data = image_slices_for_time_data[..., slice_index]  # 2D tensor
            image_slice_data = np.array(image_slice_data)
            mean_slice_ = mean_slice(image_slice_data)
            if mean_slice_ > 0.0:
                mean_slices[time_index] = (k*mean_slices[time_index] + mean_slice_) / (k + 1.0)
                k += 1.0

    return mean_slices


def compute_global_mean_slices(data_path):
    data_filenames = [item for item in os.listdir(data_path)
                      if os.path.isfile(os.path.join(data_path, item))
                      and item[0] is not '.']
    k = 0.0
    mean_slices = np.array([0.0, 0.0, 0.0, 0.0])
    for data_filename in tqdm(data_filenames):
        data_full_name = os.path.join(data_path, data_filename)
        image_slices_times = ni_image.load_img(data_full_name, dtype='float32')
        mean_slice = compute_mean_slices(image_slices_times)
        mean_slices = (k * mean_slices + mean_slice) / (k + 1.0)
        k += 1.0
    return mean_slices


def instance_normalize_slice(slice):
    non_zero = slice
    if np.mean(slice) == 0.0:
        non_zero_mean = 0.0
    else:
        # replace zeros with nan so that we don't count them in mean calculation
        non_zero[slice == 0] = np.nan
        non_zero_mean = np.nanmean(non_zero)
    # print('mean = {}'.format(non_zero_mean))
    return (slice - non_zero_mean) / 1461.0


def slices_from_slices_times(image_slices_times, slice_index, num_time_index,
                             min_volume, max_volume, slicing_mode, clahe=None):
    # for a given slice_index, this has 4 images. Shape is: (w, w, 4). 4 channels are for time indeces
    w = image_slices_times.shape[0]
    h = image_slices_times.shape[1]
    d = image_slices_times.shape[2]
    # print('shape = {}'.format(image_slices_times.shape))
    if slicing_mode == 0:
        image_slice_4ch = np.zeros(shape=(w, h, num_time_index), dtype=np.float32)
    elif slicing_mode == 1:
        image_slice_4ch = np.zeros(shape=(w, d, num_time_index), dtype=np.float32)
    elif slicing_mode == 2:
        image_slice_4ch = np.zeros(shape=(h, d, num_time_index), dtype=np.float32)

    for time_index in range(num_time_index):
        if num_time_index > 1:
            image_slices_for_time_data = image_slices_times[..., time_index]
            min_value = min_volume[time_index]
            max_value = max_volume[time_index]
        else:
            image_slices_for_time_data = image_slices_times
            min_value = min_volume
            max_value = max_volume

        if slicing_mode == 0:
            slice_index = slice_index % image_slices_for_time_data.shape[2]
            image_slice_data = image_slices_for_time_data[:, :, slice_index]  # 2D tensor
        elif slicing_mode == 1:
            slice_index = slice_index % image_slices_for_time_data.shape[1]
            image_slice_data = image_slices_for_time_data[:, slice_index, :]  # 2D tensor
        elif slicing_mode == 2:
            slice_index = slice_index % image_slices_for_time_data.shape[0]
            image_slice_data = image_slices_for_time_data[slice_index, :, :]  # 2D tensor

        image_slice_data = np.array(image_slice_data)

        if not (clahe is None):
            image_slice_data = do_clahe(slice=image_slice_data, clahe=clahe)
            print('CLAHE is used')

        # normalize it
        if len(image_slice_data.shape) == 3:
            # print('image_slice_data shape = {}, slice mode = {}'.format(image_slice_data.shape, slicing_mode))
            image_slice_data = np.squeeze(image_slice_data, axis=-1)
        temp = normalize_slice(slice=image_slice_data, min_volume=min_value,
                                                           max_volume=max_value)
        # print('normalize_slice shape = {}'.format(temp.shape))
        image_slice_4ch[..., time_index] = temp
        # if num_time_index == 1:
        #     image_slice_4ch = normalize_slice(slice=image_slice_data,
        #                                                        min_volume=min_value,
        #                                                        max_volume=max_value)
        # else:
        #     image_slice_4ch[..., time_index] = normalize_slice(slice=image_slice_data,
        #                                                        min_volume=min_value,
        #                                                        max_volume=max_value)
    return image_slice_4ch


def normalize_slice(slice, min_volume, max_volume):
    # normalize to roughly (-0.5 and 0.5) range
    return (slice - min_volume) / (max_volume - min_volume) - 0.5


def convert_slice_to_heatmap(image, min_value, max_value, should_normalize=False):
    image = (normalize_slice(slice=image, min_volume=min_value, max_volume=max_value) + 0.5) * 255.0
    # image = (image - min_value)/(max_value - min_value)*255.0
    image = np.array(image).astype(np.uint8)
    heat = cv2.applyColorMap(image, cv2.COLORMAP_JET)
    if should_normalize:
        heat = np.array(heat).astype(np.float32)
        image_mean = heat.mean(axis=(0, 1))
        heat = (heat - image_mean) / 255.0

    return heat


def overlay(input_image, mask):
    # mask = np.stack((mask,) * 3, -1)  # convert to 3 channels
    z = np.zeros_like(mask)
    mask_3ch = np.stack((z, mask, z), -1)
    overlay_image = mask_3ch * 0.5 + input_image * 1.0
    return overlay_image

def mean_sqr_batch(diff):
    dims = list(range(1, K.ndim(diff)))
    # print('dims = ', dims)
    # m = K.expand_dims(K.sqrt(K.mean(diff**2, dims)), 1)
    m =  K.expand_dims((K.mean(diff**2, dims)), 1)
    # m = K.sqrt(K.mean(diff**2, dims))
    # print ('***** mean_sqr_batch.shape: ', K.int_shape(m))
    return m
    

def mean_batch(diff):
    dims = list(range(1,K.ndim(diff)))
    # print('dims = ', dims)
    m = K.expand_dims(K.mean(diff, dims), 1)
    return m


def pad_reflect(x, padding=1):
    return tf.pad(
      x, [[0, 0], [padding, padding], [padding, padding], [0, 0]],
      mode='REFLECT')


def Conv2DReflect(*args, **kwargs):
    return Lambda(lambda x: Conv2D(*args, **kwargs)(pad_reflect(x)))


def mse(x):
    #Mean Squared Error
    return tf.reduce_mean(tf.square(x))


def sse(x,y):
    #Sum of Squared Error
    return K.expand_dims(tf.reduce_sum(tf.square(x-y)), 1)


def mse_batch(x,y):
    #Sum of Squared Error
    diff = x - y
    dims = list(range(1, K.ndim(diff)))
    # print('dims = ', dims)
    m = K.expand_dims(K.mean(diff**2, dims), 1)
    # m = K.expand_dims( ( K.sum(K.square(diff), dims) ), 1)
    # m = K.mean(K.square(diff), dims)
    # print ('***** sse_batch: style_loss.shape: ', K.int_shape(m))
    return m

def sse_batch(x,y):
    diff = x - y
    dims = list(range(1,K.ndim(diff)))
    m =  K.expand_dims( ( K.sum(K.square(diff), dims) ), 1)
    return m

def rolling_block(A, block=(3, 3)):
    shape = (A.shape[0] - block[0] + 1, A.shape[1] - block[1] + 1) + block
    strides = (A.strides[0], A.strides[1]) + A.strides
    return as_strided(A, shape=shape, strides=strides)


class ReflectionPadding2D(Layer):
    def __init__(self, padding=(1, 1), **kwargs):
        self.padding = tuple(padding)
        self.input_spec = [InputSpec(ndim=4)]
        super(ReflectionPadding2D, self).__init__(**kwargs)

    def get_output_shape_for(self, s):
        return (s[0], s[1] + 2 * self.padding[0], s[2] + 2 * self.padding[1], s[3])

    def call(self, x, mask=None):
        import tensorflow as tf
        w_pad, h_pad = self.padding
        return tf.pad(x, [[0, 0], [h_pad, h_pad], [w_pad, w_pad], [0, 0]], 'REFLECT')


def conv_block_no_BN(input_tensor, kernel_size, filters, stage, block, strides=(2, 2)):
    """A block that has a conv layer at shortcut.  Adapted from Resnet50 by removing BN layers

    # Arguments
        input_tensor: input tensor
        kernel_size: default 3, the kernel size of middle conv layer at main path
        filters: list of integers, the filters of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
        strides: Strides for the first conv layer in the block.

    # Returns
        Output tensor for the block.

    Note that from stage 3,
    the first conv layer at main path is with strides=(2, 2)
    And the shortcut should have strides=(2, 2) as well
    """
    filters1, filters2, filters3 = filters
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = Conv2D(filters1, (1, 1), strides=strides,
               name=conv_name_base + '2a')(input_tensor)
    x = Activation('relu')(x)

    x = Conv2D(filters2, kernel_size, padding='same',
               name=conv_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters3, (1, 1), name=conv_name_base + '2c')(x)

    shortcut = Conv2D(filters3, (1, 1), strides=strides,
                      name=conv_name_base + '1')(input_tensor)

    x = add([x, shortcut])
    x = Activation('relu')(x)
    return x


def res_crop_block(ip, nf=64):
    x = conv_block(ip, nf, 3, (1,1), 'valid')
    x = conv_block(x,  nf, 3, (1,1), 'valid', False)
    ip = Lambda(lambda x: x[:, 2:-2, 2:-2])(ip)

    return add([x, ip])


def up_block(x, filters, size):
    x = keras.layers.UpSampling2D()(x)
    x = Conv2D(filters, (size, size), padding='same', kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    return Activation('relu')(x)


def conv_block(x, filters, size, stride=(2,2), mode='same', act=True):
    x = Conv2D(filters, (size, size), strides=stride, padding=mode, kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    return Activation('relu')(x) if act else x


def get_outp(m, ln):
    return m.get_layer('block'+str(ln)+'_conv2').output


def gram_matrix_b(x):
    x = K.permute_dimensions(x, (0, 3, 1, 2))
    s = K.shape(x)
    feat = K.reshape(x, (s[0], s[1], s[2]*s[3]))
    return K.batch_dot(feat, K.permute_dimensions(feat, (0, 2, 1))
                      ) / K.prod(K.cast(s[1:], K.floatx()))


def mean_sqr_b(diff):
    dims = list(range(1,K.ndim(diff)))
    return tf.reduce_mean(K.expand_dims(K.sqrt(K.mean(diff**2, dims)), 0))

# preproc = lambda x: (x - rn_mean)[:, :, :, ::-1]


def preproc(x):
    rn_mean = np.array([123.68, 116.779, 103.939], dtype=np.float32)
    # rn_mean /= https://gitlab.com/lesswire1/arbitrary_style_xfer/blob/master/utils.py#L131255.0
    return (x*255.0 - rn_mean)[:, :, :, ::-1]

### Image helpers


def get_files(img_dir):
    files = os.listdir(img_dir)
    paths = []
    for x in files:
        paths.append(os.path.join(img_dir, x))
    # return [os.path.join(img_dir,x) for x in files]
    return paths


def save_img(out_path, img):
    img = np.clip(img, 0, 255).astype(np.uint8)
    scipy.misc.imsave(out_path, img)


def get_img(src):
    img = scipy.misc.imread(src, mode='RGB')
    if not (len(img.shape) == 3 and img.shape[2] == 3):
        img = np.dstack((img, img, img))
    return img


def center_crop(img, size=256):
    height, width = img.shape[0], img.shape[1]

    if height < size or width < size:  # Upscale to size if one side is too small
        img = resize_to(img, resize=size)
        height, width = img.shape[0], img.shape[1]

    h_off = (height - size) // 2
    w_off = (width - size) // 2
    return img[h_off:h_off + size, w_off:w_off + size]


def resize_to(img, resize=512):
    '''Resize short side to target size and preserve aspect ratio'''
    height, width = img.shape[0], img.shape[1]
    if height < width:
        ratio = height / resize
        long_side = int(round(width / ratio))
        resize_shape = (resize, long_side, 3)
    else:
        ratio = width / resize
        long_side = int(round(height / ratio))
        resize_shape = (long_side, resize, 3)

    return scipy.misc.imresize(img, resize_shape)


def get_img_crop(src, resize=512, crop=256):
    '''Get & resize slice and center crop'''
    img = get_img(src)
    img = resize_to(img, resize)
    return center_crop(img, crop)


def get_img_random_crop(src, resize=512, crop=256):
    '''Get & resize slice and random crop'''
    img = get_img(src)
    img = resize_to(img, resize=resize)

    offset_h = random.randint(0, (img.shape[0] - crop))
    offset_w = random.randint(0, (img.shape[1] - crop))

    img = img[offset_h:offset_h + crop, offset_w:offset_w + crop, :]

    return img


def save_as_hdf5(image_filenames, df5_filename, dataset_name, data_shape):
    with h5py.File(df5_filename, "a") as f:
        idx = 0
        _, image_width, image_height, _ = data_shape
        data = f.create_dataset(dataset_name, dtype='i8', shape=data_shape)
        for image_name in tqdm(image_filenames):
            im = cv2.imread(image_name)
            im = centered_crop(im, image_width, image_height)
            data[idx, ...] = im
            idx += 1

    print('saved ' + df5_filename)

def create_df5_from_files(data_path, df5_training_filename, df5_validation_filename,
                          image_width=288, image_height=288):

    if not exists('./data'):
        makedirs('./data')

    if exists(df5_training_filename) and exists(df5_validation_filename):
        print(df5_training_filename + ' exist already. Loading it...')
    else:
        print('creating df5 file for images...')
        folder_names = [join(data_path, d) for d in listdir(data_path) if isdir(join(data_path, d))]
        # folder_names = folder_names[:2]
        image_names = []
        print('creating filenames...')
        for folder_name in tqdm(folder_names):
            names = [join(folder_name, f) for f in listdir(folder_name) if isfile(join(folder_name, f))]
            image_names.append(names)

        image_names_flat = []
        for sublist in image_names:
            for item in sublist:
                image_names_flat.append(item)

        image_names = np.array(image_names_flat)

        num_images = image_names.shape[0]
        print('Total number of images = ', num_images)
        np.random.shuffle(image_names)
        num_images /= 2
        print('Reduced number of images = ', num_images)
        image_names = image_names[:num_images]

        training_fraction = 0.9
        num_training_samples = int(num_images * training_fraction)
        num_training_samples = (num_training_samples // 8) * 8   # so that we can do multi-gpu
        num_validation_samples = num_images - num_training_samples
        num_validation_samples = (num_validation_samples // 8) * 8   # so that we can do multi-gpu

        print('creating training set...')
        training_data_shape = [num_training_samples, image_width, image_height, 3]
        save_as_hdf5(image_filenames=image_names[:num_training_samples],
                     df5_filename=df5_training_filename,
                     dataset_name='images',
                     data_shape=training_data_shape)

        print('creating validation set...')
        validation_data_shape = [num_validation_samples, image_width, image_height, 3]
        save_as_hdf5(image_filenames=image_names[num_training_samples:num_training_samples+num_validation_samples],
                     df5_filename=df5_validation_filename,
                     dataset_name='images',
                     data_shape=validation_data_shape)