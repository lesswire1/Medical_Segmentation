import DataGenerator
import model as mode_lib
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
import keras
import time
import os
from keras.optimizers import Adam, RMSprop
from keras import metrics
# import utils
import numpy as np
# import skimage.io as io
from os import environ, makedirs
import cv2
from keras import losses
from nilearn import image as ni_image
# from scipy.misc.pilutil import imresize
import utils
import test_model
import keras.metrics
from keras.models import load_model
from keras.utils.generic_utils import CustomObjectScope
import functools
from keras import backend as K
import random

# environ['CUDA_VISIBLE_DEVICES'] ='3'


class epoch_callback(keras.callbacks.Callback):
    def __init__(self, slicing_mode, metadata, data_dir, label_dir, validation_file_idx, do_clahe):
        super(epoch_callback, self).__init__()
        self.slicing_mode = slicing_mode
        self.metadata = metadata
        self.do_clahe = do_clahe
        self.data_dir = data_dir
        self.label_dir = label_dir
        self.validation_file_idx = validation_file_idx

    def on_train_begin(self, logs={}):
        print('Training started...')
        return

    def on_train_end(self, logs={}):
        print('Training ended.')
        utils.notification('Training Ended!')
        return

    def on_epoch_begin(self, epoch, logs={}):
        return

    def on_epoch_end(self, epoch, logs={}):
        result_path = '../Decalon/results'
        if not os.path.exists(result_path):
            makedirs(result_path)

        model_path = '../Decalon/checkpoints/model_at_epoch_%d.h5' % epoch
        if not os.path.exists('../Decalon/checkpoints'):
            makedirs('../Decalon/checkpoints')

        print('epoch end: saving model...')
        self.model.save(model_path)
        print('epoch end: predicting...')
        test_model.evaluate_labels(image_data_folder=self.data_dir, label_folder=self.label_dir,
                                   model=self.model, file_index=self.validation_file_idx,
                                   metadata=self.metadata, do_clahe=False, result_folder=result_path)

        # test_model.predict_labels_for_validation_sample(image_data_folder=self.data_dir,
        #                                                 label_folder=self.label_dir,
        #                                                 file_index=self.validation_file_idx,
        #                                                 metadata=self.metadata,
        #                                                 result_folder=result_path,
        #                                                 model_path=model_path,
        #                                                 do_clahe=self.do_clahe)

        return

    def on_batch_begin(self, batch, logs={}):
        return

    def on_batch_end(self, batch, logs={}):
        # self.losses.append(logs.get('loss'))
        return


def model_fit(slicing_mode, batch_size=16, log_name='brain_seg_combined_time_full',
              should_load_model=False,
              model_path='./models/w1222g0_5ch_90prec.h5',
              image_data_folder='./Data/Task01_BrainTumour/imagesTr',
              label_folder='./Data/Task01_BrainTumour/labelsTr',
              metadata=None,
              is_debug=False,
              do_clahe=False):

    num_input_channels = metadata.item().get('num_input_channels')
    num_output_channels = len(metadata.item().get('output_label_values'))
    num_slices_array = metadata.item().get('num_slices_array')
    slice_width = metadata.item().get('slice_width')

    label_fractions = metadata.item().get('mean_fraction_levels')
    print('             label_fractions = {}'.format(label_fractions))

    label_fractions = np.array(label_fractions / label_fractions[0])

    ratio = label_fractions[0] / (label_fractions + 10.0e-4)

    min_fraction = np.min(label_fractions)
    # max_weight = 0.02 / (num_output_channels - 1) * np.log(label_fractions[0] / (min_fraction + 8.0e-4))
    max_weight = 0.05 / (num_output_channels - 1) * np.log(label_fractions[0] / (min_fraction + 8.0e-4))
    scale_factor = max_weight / min_fraction
    class_weights = min_fraction / label_fractions * scale_factor
    class_weights[0] = 1.0

    # pwr = 1.0  # 2.0 / (num_output_channels - 1)
    # class_weights = 10.0 * (num_output_channels - 1) * np.power(ratio, pwr)
    #
    # class_weights[1:] = 20.0 / np.power((num_output_channels - 1), 1.0) * np.log(class_weights[1:])
    # class_weights[1] /= 3.5

    # class_weights = 1.0 + 4.0*np.log(ratio)

    # mean_foreground_fraction = np.max(class_weights[1:])
    # class_weights[1:] = mean_foreground_fraction


    # temp = 25.0 * num_output_channels  # higher temp makes the class weight more equal
    # exp_w = np.exp(class_weights/temp)
    # class_weights = exp_w / np.sum(exp_w)
    # class_weights = class_weights / class_weights[0]

    # class_weights = np.exp(-label_fractions*8.0)
    # class_weights = class_weights / class_weights[0]*100.0

    print('         ****   class_weights = {}'.format(class_weights))
    partial_loss = functools.partial(utils.pixelwise_crossentropy, class_weights=class_weights)
    partial_loss.__name__ = "partial_loss"

    def relu6(x):
        return K.relu(x, max_value=6)

    if should_load_model:
        with CustomObjectScope({'relu6': relu6,
                                'DepthwiseConv2D': keras.layers.DepthwiseConv2D,
                                'partial_loss': partial_loss,
                                'tversky_loss': utils.tversky_loss,
                                'pixelwise_crossentropy': utils.pixelwise_crossentropy}):
            model = load_model(model_path)
        for l in model.layers:
            l.trainable = True
    else:
        model = mode_lib.mobilenet_model_segmentation(input_shape=(224, 224, num_input_channels),
                                                      num_output_channels=num_output_channels)

    opt = Adam(lr=1.0e-5)
    # opt = RMSprop(lr=1.0e-4)
    # model.compile(loss=utils.tversky_loss, optimizer=opt)
    model.compile(loss=partial_loss, optimizer=opt)

    data_filenames_ordered = [item for item in os.listdir(image_data_folder)
                      if os.path.isfile(os.path.join(image_data_folder, item))
                      and item[0] is not '.']


    idx = np.random.permutation(len(data_filenames_ordered))
    data_filenames = np.array(data_filenames_ordered)[idx]
    num_slices_array = np.array(num_slices_array)[idx]
    print("num_volumes = {}, num_slice_array = {}".format(len(num_slices_array), num_slices_array))

    # ************ DEBUG
    if is_debug:
        print("*****  DEBUG Mode *****")
        num_files_debug = 40
        data_filenames = data_filenames[:num_files_debug]  # ************ DEBUG
        num_slices_array = num_slices_array[:num_files_debug]

    num_volumes = len(data_filenames)
    num_training_volumes = int(num_volumes * 0.95)
    num_validation_volumes = num_volumes - num_training_volumes

    training_filenames = data_filenames[:num_training_volumes]
    validation_filenames = data_filenames[num_training_volumes:]

    validation_index = data_filenames_ordered.index(validation_filenames[1])
    print('end of epoch test filename = {}'.format(validation_filenames[1]))

    # aug_factor = 1
    aug_factor = 1 if len(data_filenames) > 200 else 2   #  just vertical flipping because images are vertically symmetric
    print('**     Augmentation factor = {}'.format(aug_factor))

    should_do_lateral_augmentation = 0 # (len(data_filenames) < 400) * 1.0
    print('**     Augmenting training data by slicing laterally: {}'.format(should_do_lateral_augmentation))

    num_training_samples =  np.sum(num_slices_array[:num_training_volumes]) + \
                            should_do_lateral_augmentation * num_training_volumes * slice_width * 2

    num_validation_samples = np.sum(num_slices_array[num_training_volumes:]) + \
                             should_do_lateral_augmentation * num_validation_volumes * slice_width * 2

    num_training_samples *= aug_factor
    num_validation_samples *= aug_factor

    num_validation_samples = num_validation_samples - (num_validation_samples % batch_size)
    num_training_samples = num_training_samples - (num_training_samples % batch_size)

    training_batch_gen = DataGenerator.SeqBatchGen(image_data_folder=image_data_folder,
                                                   image_label_folder=label_folder,
                                                   data_filenames=training_filenames,
                                                   num_samples=num_training_samples,
                                                   metadata=metadata,
                                                   batch_size=batch_size,
                                                   slicing_mode=slicing_mode,
                                                   aug_factor=aug_factor,
                                                   num_slices_array=num_slices_array[:num_training_volumes],
                                                   slice_width=slice_width,
                                                   do_clahe=do_clahe)

    validation_batch_gen = DataGenerator.SeqBatchGen(image_data_folder=image_data_folder,
                                                     image_label_folder=label_folder,
                                                     data_filenames=validation_filenames,
                                                     num_samples=num_validation_samples,
                                                     metadata=metadata,
                                                     batch_size=batch_size,
                                                     slicing_mode=slicing_mode,
                                                     aug_factor=aug_factor,
                                                     num_slices_array=num_slices_array[num_training_volumes:],
                                                     slice_width=slice_width,
                                                     do_clahe=do_clahe)

    print('number of training samples = {}'.format(num_training_samples))
    print('number of validation samples = {}'.format(num_validation_samples))
    print('batch size = {}'.format(batch_size))

    es = EarlyStopping(monitor='val_loss', patience=20, verbose=1)
    t = time.localtime()
    time_str = str(t.tm_mon) + '_' + str(t.tm_mday) + '_' + str(t.tm_hour) + '_' + str(t.tm_min)
    graph_path = '../Decalon/Graph/'
    if not os.path.exists(graph_path):
        makedirs(graph_path)
    tsb = keras.callbacks.TensorBoard(log_dir=graph_path + log_name + '_' + time_str)
    lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=3, verbose=1, mode='auto', min_delta=1.0e-5,
                           cooldown=0, min_lr=0)

    # data_dir = './Data/callback_test/data'
    # label_dir = './Data/callback_test/label'
    image_write_callback = epoch_callback(slicing_mode=slicing_mode,
                                          metadata=metadata,
                                          data_dir=image_data_folder,
                                          label_dir=label_folder,
                                          validation_file_idx=validation_index,
                                          do_clahe=do_clahe)

    model.fit_generator(generator=training_batch_gen, steps_per_epoch=int(num_training_samples // batch_size),
                        epochs=100, verbose=1, callbacks=[image_write_callback, tsb, lr, es],
                        validation_data=validation_batch_gen,
                        validation_steps=int(num_validation_samples // batch_size),
                        workers=8, max_queue_size=250, use_multiprocessing=False,
                        shuffle=True)


