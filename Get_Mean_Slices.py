import utils
import numpy as np

image_data_folder = './Data/Task01_BrainTumour/imagesTr'
mean_slices = utils.compute_global_mean_slices(data_path=image_data_folder)
np.save('./mean_slices', mean_slices)
print('saved global mean slices!')
