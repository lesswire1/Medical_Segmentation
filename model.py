from keras.layers import Input, UpSampling2D, Dropout
from keras.models import Model
from keras.layers.convolutional import Conv2D
from keras.layers.core import Activation
from keras.applications.mobilenet import MobileNet
from keras.layers import Conv2DTranspose, BatchNormalization, Concatenate, concatenate, DepthwiseConv2D
import utils
from keras.layers import Lambda
from keras.applications.inception_v3 import InceptionV3
from keras.applications.xception import Xception
import keras

def separable_conv_block(x, size, stride=(2,2), mode='same', act=True):
    x = DepthwiseConv2D(kernel_size=size, strides=stride, padding=mode)(x)
    x = BatchNormalization()(x)
    return Activation('relu')(x) if act else x


def up2x(x):
    # return UpSampling2D()(x)
    x = Conv2DTranspose(filters=124, kernel_size=(4, 4), strides=(2, 2),
                        kernel_initializer='he_normal', padding='same')(x)  # 20 x 14
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    return x


def concatAndUp2x(l1, l2):
    out = concatenate([l1, l2])
    return up2x(out)


def mobilenet_model_segmentation(input_shape=(224, 224, 4), num_output_channels=1):
    model_input = Input(shape=input_shape)

    # convert 4ch or 1ch input to 3ch which mobilenet needs
    input_3ch = Conv2D(3, (1, 1), kernel_initializer='he_normal', activation='linear', padding='same')(model_input)

    mobilenet_input = Input(shape=(224, 224, 3))
    mobilenet_model = MobileNet(input_shape=(224, 224, 3),
                                # dropout=0.5,
                                alpha=1.0,
                                include_top=False,
                                input_tensor=mobilenet_input)

    for l in mobilenet_model.layers:
        l.trainable = True

    # print(mobilenet_model.summary())

    # cut the last few layer of Mobilenet
    side_layer_names = ['conv_pw_5_relu', 'conv_pw_3_relu', 'conv_pw_1_relu']   # [28, 56, 112]
    side_layers = [mobilenet_model.get_layer(l_name).output for l_name in side_layer_names]

    # out_mn = mobilenet_model.get_layer('conv_pw_5_relu').output    # 28x28
    truncated_mobilenet_model = Model(inputs=mobilenet_input, outputs=side_layers)

    out_side_layers = truncated_mobilenet_model(input_3ch)

    # add our own custom layers
    out = out_side_layers[0]
    for k in range(5):
        out = separable_conv_block(out, size=(5, 5), stride=(1, 1))
        # out = Dropout(0.2)(out)

    for k in range(5):
        out = separable_conv_block(out, size=(3, 3), stride=(1, 1))
        # out = Dropout(0.2)(out)

    # upsize 8x to 224x224 with concatenation
    for k in range(3):
        out = concatAndUp2x(out_side_layers[k], out)
        # out = Dropout(0.2)(out)

    out = Conv2D(num_output_channels, (3, 3), kernel_initializer='he_normal', activation='linear', padding='same')(out)

    out = Lambda(utils.depth_softmax)(out)

    model_ = Model(inputs=model_input, outputs=out)
    return model_

def xception_model_segmentation(input_shape=(216, 216, 4), num_output_channels=1):
    model_input = Input(shape=input_shape)

    # convert 4ch or 1ch input to 3ch which mobilenet needs
    input_3ch = Conv2D(3, (1, 1), kernel_initializer='he_normal', activation='linear', padding='same')(model_input)

    w = input_shape[0]
    inception_input_shape = (w, w, 3)
    inception_input = Input(shape=inception_input_shape)
    inception_model = Xception(include_top=False, input_shape=inception_input_shape, input_tensor=inception_input)

    for l in inception_model.layers:
        l.trainable = True

    # print(inception_model.summary())

    # cut the last few layer of Mobilenet
    # side_layer_names = ['block4_sepconv2_bn']   # [12]
    # side_layers = [inception_model.get_layer(l_name).output for l_name in side_layer_names]

    out_pretrained = inception_model.get_layer('block13_sepconv2_bn').output    # 14x14
    truncated_pre_trained_model = Model(inputs=inception_input, outputs=out_pretrained)

    out = truncated_pre_trained_model(input_3ch)

    # add our own custom layers
    # for k in range(5):
    out = separable_conv_block(out, size=(3, 3), stride=(1, 1))
    out = Dropout(0.2)(out)

    # upsize 8x to 224x224 with concatenation
    for k in range(4):
        # out = concatAndUp2x(out_side_layers[k], out)
        out = up2x(out)
        out = Dropout(0.2)(out)

    out = Conv2D(num_output_channels, (3, 3), kernel_initializer='he_normal', activation='linear', padding='same')(out)

    out = Lambda(utils.depth_softmax)(out)

    model_ = Model(inputs=model_input, outputs=out)
    return model_

if __name__ == "__main__":
    # model = mobilenet_model_segmentation()
    model = xception_model_segmentation(input_shape=(224, 224, 4))
    print(model.summary())

