from nilearn import image as ni_image
import cv2
import numpy as np
import os
from tqdm import tqdm
import utils
from sklearn.utils import class_weight


def generate_metadata_for_task(task_path, metadata_path, should_visualize=False):
    if not os.path.exists(metadata_path):
        os.makedirs(metadata_path)

    task = os.path.split(task_path)
    meta_data_name = os.path.join(metadata_path, 'meta_' + task[-1] + '.npy')
    print('writing to {}'.format(meta_data_name))
    image_data_folder = os.path.join(task_path, 'imagesTr')
    label_data_folder = os.path.join(task_path, 'labelsTr')
    test_data_folder = os.path.join(task_path, 'imagesTs')

    test_data_filenames = [item for item in os.listdir(test_data_folder)
                      if os.path.isfile(os.path.join(test_data_folder, item))
                      and item[0] is not '.']

    data_filenames = [item for item in os.listdir(image_data_folder)
                      if os.path.isfile(os.path.join(image_data_folder, item))
                      and item[0] is not '.']

    image_data_folder_224 = os.path.join(task_path, 'imagesTr_224_u')
    label_data_folder_224 = os.path.join(task_path, 'labelsTr_224_u')
    test_data_folder_224 = os.path.join(task_path, 'imagesTs_224_u')

    if not os.path.exists(image_data_folder_224):
        os.makedirs(image_data_folder_224)
    if not os.path.exists(label_data_folder_224):
        os.makedirs(label_data_folder_224)
    if not os.path.exists(test_data_folder_224):
        os.makedirs(test_data_folder_224)

    data_full_name = os.path.join(image_data_folder, data_filenames[0])
    label_full_name = os.path.join(label_data_folder, data_filenames[0])

    image_slices_times = ni_image.load_img(data_full_name, dtype='float32').get_data()
    labels = ni_image.load_img(label_full_name, dtype='float32').get_data()

    slice_width = 224 # image_slices_times.shape[0]
    original_slice_width = image_slices_times.shape[0]
    print('original volume shape = {}'.format(image_slices_times.shape))
    if len(image_slices_times.shape) == 3:
        image_slices_times = np.expand_dims(image_slices_times, axis=-1)
    num_input_channels = image_slices_times.shape[3]
    print('num input channels = {}'.format(num_input_channels))
    labels_flat = np.reshape(labels, newshape=(1, -1))
    labels_flat = np.squeeze(labels_flat)
    label_vals = set(labels_flat)
    print('label values = {}'.format(label_vals))
    # max_label_val = np.max(labels)
    # print('max lavel value = {}'.format(max_label_val))
    #
    # max_data_val = np.max(image_slices_times, axis=(0, 1, 2, 3))
    # print('max data value = {}'.format(max_data_val))
    #
    # min_data_val = np.min(image_slices_times, axis=(0, 1, 2, 3))
    # print('min data value = {}'.format(min_data_val))

    num_slices_array = []
    mean_input_channels = np.zeros(shape=(num_input_channels))

    levels = np.sort(np.array(list(label_vals)))
    N = 0.0
    mean_fraction_levels = np.zeros(shape=len(levels))

    # DEBUG
    # data_filenames = data_filenames[:11]

    print('converting test images to 224x224 and uncompressed')
    for filename in tqdm(test_data_filenames):
        data_full_name = os.path.join(test_data_folder, filename)
        image_slices_times_ = ni_image.load_img(data_full_name, dtype='float32')
        image_slices_times = image_slices_times_.get_data()

        if len(image_slices_times.shape) == 3:
            image_slices_times = np.expand_dims(image_slices_times, axis=-1)

        num_slices = image_slices_times.shape[2]
        if num_input_channels > 1:
            volume_224 = np.zeros(shape=(224, 224, num_slices, num_input_channels))
        else:
            volume_224 = np.zeros(shape=(224, 224, num_slices))

        for time_idx in range(num_input_channels):
            for k in range(num_slices):
                slice = image_slices_times[..., k, time_idx]
                slice = cv2.resize(slice, (224, 224), interpolation=cv2.INTER_CUBIC)
                if num_input_channels > 1:
                    volume_224[..., k, time_idx] = slice
                else:
                    volume_224[..., k] = slice

        image_224_volume = ni_image.new_img_like(ref_niimg=image_slices_times_, data=volume_224, copy_header=False)
        image_224_full_name = os.path.join(test_data_folder_224, filename)
        image_224_full_name = image_224_full_name.replace('.gz', '')
        image_224_volume.to_filename(image_224_full_name)


    for filename in tqdm(data_filenames):
        data_full_name = os.path.join(image_data_folder, filename)
        label_full_name = os.path.join(label_data_folder, filename)

        image_slices_times_ = ni_image.load_img(data_full_name, dtype='float32')
        image_slices_times = image_slices_times_.get_data()
        # print('volume shape = {}'.format(image_slices_times.shape))
        labels_ = ni_image.load_img(label_full_name, dtype='float32')
        labels = labels_.get_data()

        data_axis = (0, 1, 2, 3) if num_input_channels == 4 else (0, 1, 2)
        max_data_val = np.percentile(image_slices_times, q=99, axis=data_axis)
        # print('99 percentile value = {}'.format(max_data_val))

        min_data_val = np.min(image_slices_times, axis=data_axis)
        # print('min data value = {}'.format(min_data_val))

        if len(image_slices_times.shape) == 3:
            image_slices_times = np.expand_dims(image_slices_times, axis=-1)

        num_slices = image_slices_times.shape[2]
        num_slices_array.append(num_slices)

        total_labels = np.prod(labels.shape)
        fraction_levels = np.zeros(shape=len(levels))
        for k, level in enumerate(levels):
            num_this_label = np.sum((labels == level)*1.0)
            fraction_levels[k] = num_this_label / total_labels
        mean_fraction_levels = (N*mean_fraction_levels + fraction_levels) / (N+1.0)
        N += 1.0

        # # DEBUG
        # print(set(labels_flat), set(label_vals))
        # class_weights = class_weight.compute_class_weight('balanced', np.unique(labels_flat), labels_flat)
        # class_weights = class_weights / class_weights[0]
        # print('class_wights = {}'.format(class_weights))

        if num_input_channels > 1:
            volume_224 = np.zeros(shape=(224, 224, num_slices, num_input_channels))
        else:
            volume_224 = np.zeros(shape=(224, 224, num_slices))
        label_224 = np.zeros(shape=(224, 224, num_slices))

        for time_idx in range(num_input_channels):
            for k in range(num_slices):
                slice = image_slices_times[..., k, time_idx]
                slice = cv2.resize(slice, (224, 224), interpolation=cv2.INTER_CUBIC)
                if num_input_channels > 1:
                    volume_224[..., k, time_idx] = slice
                else:
                    volume_224[..., k] = slice
                label = labels[..., k]
                label = cv2.resize(label, (224, 224), interpolation=cv2.INTER_NEAREST)
                label_224[..., k] = label

                if should_visualize:
                    max_label_val = np.max(list(label_vals))
                    label_u8 = (label/max_label_val*255.0).astype(np.uint8)
                    heat_label = cv2.applyColorMap(label_u8, cv2.COLORMAP_JET)
                    heat = utils.convert_slice_to_heatmap(slice, min_value=min_data_val,
                                                          max_value=max_data_val,
                                                          should_normalize=False)
                    combined = np.hstack((heat, heat_label))
                    title = filename + ' ' + str(time_idx) + ', ' + str(k)
                    font = cv2.FONT_HERSHEY_SIMPLEX
                    cv2.putText(combined, title, org=(10, 20), fontFace=font, fontScale=0.5, color=(255, 255, 255))
                    cv2.imshow('test', combined)
                    cv2.waitKey(1)

        label_224_volume = ni_image.new_img_like(ref_niimg=labels_, data=label_224, copy_header=False)
        label_224_full_name = os.path.join(label_data_folder_224, filename)
        label_224_full_name = label_224_full_name.replace('.gz', '')
        label_224_volume.to_filename(label_224_full_name)

        image_224_volume = ni_image.new_img_like(ref_niimg=image_slices_times_, data=volume_224, copy_header=False)
        image_224_full_name = os.path.join(image_data_folder_224, filename)
        image_224_full_name = image_224_full_name.replace('.gz', '')
        image_224_volume.to_filename(image_224_full_name)

    print('num_slices_array = {}'.format(num_slices_array))
    print('mean_input_channels = {}'.format(mean_input_channels))
    print('mean_fraction_levels = {}'.format(mean_fraction_levels))

    test_data_filenames = [item for item in os.listdir(test_data_folder)
                      if os.path.isfile(os.path.join(test_data_folder, item))
                      and item[0] is not '.']
    num_test_slices_array = []

    for filename in tqdm(test_data_filenames):
        data_full_name = os.path.join(test_data_folder, filename)
        image_slices_times = ni_image.load_img(data_full_name, dtype='float32').get_data()
        num_slices = image_slices_times.shape[2]
        num_test_slices_array.append(num_slices)

    meta_data = {'num_input_channels': num_input_channels, 'output_label_values': label_vals,
                 'max_data_val': max_data_val, 'min_data_val': min_data_val, 'slice_width': slice_width,
                 'original_slice_width': original_slice_width,
                 'num_slices_array': num_slices_array, 'num_test_slices_array': num_test_slices_array,
                 'mean_fraction_levels': mean_fraction_levels,
                 'mean_input_channels': mean_input_channels.tolist()}

    np.save(file=meta_data_name, arr=meta_data)


def generate_metadata_for_tasks(tasks_path, metadata_path, should_visualize):
    task_folders = [os.path.join(tasks_path, item) for item in os.listdir(tasks_path)
                      if os.path.isdir(os.path.join(tasks_path, item))
                      and 'Task' in item]

    # DEBUG
    task_folders = [task_folders[6]]

    print(task_folders)
    for task_folder in task_folders:
        print('task_folder = {}'.format(task_folder))
        generate_metadata_for_task(task_path=task_folder, metadata_path=metadata_path, should_visualize=should_visualize)

if __name__ == "__main__":
    # task_path = '../../Data/Task02_Heart'
    # task_path = './Data/Task01_BrainTumour'
    # task_path = '../../Data/Task03_Liver'
    # generate_metadata_for_task(task_path=task_path, metadata_path='./Data/metadata')

    tasks_path = 'D:/ML_Data'
    generate_metadata_for_tasks(tasks_path=tasks_path,
                                metadata_path='D:/ML_Data/metadata',
                                should_visualize=False)
