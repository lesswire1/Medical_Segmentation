from nilearn import image as ni_image
import cv2
import numpy as np
import utils
import os


def convert_slice_to_heatmap(image, should_normalize=True):
    image = image/1461.0*255
    image = np.array(image).astype(np.uint8)
    heat = cv2.applyColorMap(image, cv2.COLORMAP_JET)
    if should_normalize:
        # image_mean = np.array([136.61010417, 67.11958333, 28.77125])
        image_mean = np.array([1.66598906e+02, 3.51526389e+01, 4.37500000e-03])
        heat = (np.array(heat).astype(np.float32) - image_mean) / 255.0

    return heat


data_full_name = './Data/callback_test/data/BRATS_484.nii.gz'
label_full_name = './Data/Task01_BrainTumour/labelsTr/BRATS_484.nii.gz'

image_slices_times = ni_image.load_img(data_full_name)
print(image_slices_times.shape)

time_index = 0
image_slices = ni_image.index_img(image_slices_times, time_index)
image_slices_data = image_slices.get_data()

image_224_volume = ni_image.new_img_like(ref_niimg=image_slices, data=image_slices_data, copy_header=False)
uncompressed_filename = './Data/deleteMe/my_uncompressed_484_image.nii'
image_224_volume.to_filename(uncompressed_filename)

uncompressed_data = ni_image.load_img(uncompressed_filename).get_data()

diff = image_slices_data - uncompressed_data
sum_diff = np.sum(diff)

print(sum_diff)



# # image_mean = np.array([136.61010417, 67.11958333, 28.77125])
# for k in range(70, 78):
#     image_slice = image_slices_data[..., k]
#     heat = convert_slice_to_heatmap(image_slice, should_normalize=False)
#     heat = heat # * 255 + image_mean
#     cv2.imwrite('./debug/heat_{}.png'.format(k), heat)
#
#     m = np.array(heat).mean(axis=(0, 1))
#     print('mean = {}'.format(m))




#
# slice_index = 72
# labels_slices = ni_image.load_img(label_full_name, dtype='float32')
# label_slices_data = labels_slices.get_data()
# label_sigmoid = utils.label_to_categorical(label_slices_data, slice_index, is_tensor=False, slicing_mode=0)
# print('label_sigmoid shape = {}'.format(label_sigmoid.shape))
# print('labels_sigmoid = {}, sum = {}'.format(label_sigmoid[0, 0, :], np.sum(label_sigmoid[0, 0, :])))
#
# noise = np.random.random(label_sigmoid.shape)
# label_sigmoid += noise / 4.0
# gray_label = utils.label_softmax_to_heat_image(label_sigmoid)
#
# cv2.imshow('gray', gray_label)
# cv2.waitKey(0)


