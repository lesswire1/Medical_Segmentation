
from pycocotools.coco import COCO
import numpy as np
import skimage.io as io
# import matplotlib.pyplot as plt
import pylab
import pycocotools.mask as maskUtils
import os
from tqdm import tqdm
"""
Download this and place it in ./COCO/annotations folder
wget http://images.cocodataset.org/annotations/annotations_trainval2017.zip

"""
generate_train_data = True

def annToRLE(ann, height, width):
    """
    Convert annotation which can be polygons, uncompressed RLE to RLE.
    :return: binary mask (numpy 2D array)
    """
    segm = ann['segmentation']
    if isinstance(segm, list):
        # polygon -- a single object might consist of multiple parts
        # we merge all parts into one mask rle code
        rles = maskUtils.frPyObjects(segm, height, width)
        rle = maskUtils.merge(rles)
    elif isinstance(segm['counts'], list):
        # uncompressed RLE
        rle = maskUtils.frPyObjects(segm, height, width)
    else:
        # rle
        rle = ann['segmentation']
    return rle


def annToMask(ann, height, width):
    """
    Convert annotation which can be polygons, uncompressed RLE, or RLE to binary mask.
    :return: binary mask (numpy 2D array)
    """
    rle = annToRLE(ann, height, width)
    m = maskUtils.decode(rle)
    return m


pylab.rcParams['figure.figsize'] = (8.0, 10.0)

dataDir = '/data/COCO'
if generate_train_data:
    dataType = 'train2017'
    image_mask_folder = '/data/COCO/image_mask_train'
else:
    dataType = 'val2017'
    image_mask_folder = '/data/COCO/image_mask_val'

annFile = '{}/annotations/instances_{}.json'.format(dataDir, dataType)

if not os.path.exists(image_mask_folder):
    os.makedirs(image_mask_folder)

# initialize COCO api for instance annotations
coco = COCO(annFile)

catIds = coco.getCatIds(catNms=['person'])
imgIds = coco.getImgIds(catIds=catIds)
print('number of person slice = {}'.format(len(imgIds)))
# imgIds = imgIds[0:3]

images = coco.loadImgs(imgIds)

print('num images = {}'.format(len(images)))

k = 0
old_mean = 0.0
# images = images[0:5]
for image_coco in tqdm(images):
    image = io.imread(image_coco['coco_url'])
    im_name = '{}/{}.jpg'.format(image_mask_folder, k)
    io.imsave(im_name, image)
    new_sample = np.mean(image, axis=(0, 1))
    new_mean = (old_mean*k + new_sample)/(k+1)
    old_mean = new_mean

    annIds = coco.getAnnIds(imgIds=image_coco['id'], catIds=catIds, iscrowd=None)
    annotations = coco.loadAnns(annIds)
    mask = np.zeros(image.shape[0:2])
    for annotation in annotations:
        mask += annToMask(annotation, height=image_coco['height'], width=image_coco['width'])

    mask_name = '{}/{}_mask.png'.format(image_mask_folder, k)
    mask *= 255
    mask = mask.astype(np.uint8)
    io.imsave(mask_name, mask)

    k += 1

print('mean RGB = ', new_mean)
# np.save('./mean_RGB.npy', old_mean)
